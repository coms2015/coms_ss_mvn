package study.coms.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import study.coms.exchange.message.XLoginReq;
import study.coms.exchange.message.XLogoutReq;
import study.coms.exchange.user.XUserDto;
import study.coms.server.exception.AuthException;
import study.coms.server.service.UserService;

@RestController
public class AuthController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/auth/login", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public XUserDto tryAuth(@RequestBody final XLoginReq request) {
        XUserDto response = userService.tryToAuth(request.getLogin(), request.getPassword());
        if (response != null)
            return response;
        else
            throw new AuthException();
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "/auth/logout", method = RequestMethod.POST, consumes = "application/json")
    public void logout(@RequestBody final XLogoutReq request) {
        userService.logout();
    }
}
