package study.coms.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.reflect.Constructor;
import java.util.Arrays;

@Controller
public class AuxController {
    private ObjectMapper mapper = new ObjectMapper();

    @RequestMapping(value = "/ref", method = RequestMethod.GET, produces = "text/plain")
    public @ResponseBody String getReferentialRequests(@RequestParam(value = "class") String clazz) {
        try {
            Class<?> objClass = Class.forName(clazz);
            Constructor<?> ctor = objClass.getDeclaredConstructor();
            Object o = ctor.newInstance();
            return mapper.writeValueAsString(o);
        } catch (Exception e) {
            return "Error: " + e.getClass().getName() + " " + e.getMessage() + "\n\r\n\r" + Arrays.toString(e.getStackTrace());
        }
    }
}
