package study.coms.server.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import study.coms.exchange.message.XErrorResp;
import study.coms.server.entity.EError;
import study.coms.server.exception.AbstractServerException;
import study.coms.server.service.ErrorService;

@ControllerAdvice
public final class ErrorControllerAdvice {
    @Autowired
    private ErrorService errorService;

    private static final Logger logger = LoggerFactory.getLogger(ErrorControllerAdvice.class);

    @ExceptionHandler(value = {AbstractServerException.class, JsonProcessingException.class, HibernateException.class, javax.persistence.PersistenceException.class})
    public ResponseEntity<XErrorResp> serverException(Exception e) {
        logger.error("ServerException!", e);
        EError error = errorService.getErrorByClass(e.getClass());
        if (error != null)
            return new ResponseEntity<>(new XErrorResp(error.getCode(), error.getName()), error.getHttpcode() > 0 ? HttpStatus.valueOf(error.getHttpcode()) : HttpStatus.INTERNAL_SERVER_ERROR);
        else
            return new ResponseEntity<>(new XErrorResp(-1, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
