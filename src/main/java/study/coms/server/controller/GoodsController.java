package study.coms.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import study.coms.exchange.goods.XGoodsDto;
import study.coms.server.exception.WrongIdentifierException;
import study.coms.server.service.GoodsService;

import java.util.List;

@RestController
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    @RequestMapping(value = "/user/goods", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public List<XGoodsDto> getGoods(@RequestParam(value = "store_n", defaultValue = "0") final int storeN) {
        if (storeN == 0)
            return goodsService.goods();
        else
            return goodsService.goodsByStoreN(storeN);
    }

    @RequestMapping(value = "/user/goods/add", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public void addGoods(@RequestBody final XGoodsDto goodsDto) {
        goodsService.addGoods(goodsDto);
    }

    @RequestMapping(value = "/user/goods/edit", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public void editGoods(@RequestBody final XGoodsDto goodsDto) {
        goodsService.editGoods(goodsDto);
    }

    @RequestMapping(value = "/user/goods/remove", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public void removeGoods(@RequestBody final XGoodsDto goodsDto) {
        if (goodsDto.getN() == null)
            throw new WrongIdentifierException();
        goodsService.removeGoods(goodsDto.getN());
    }
}
