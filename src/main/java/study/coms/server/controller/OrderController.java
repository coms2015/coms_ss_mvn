package study.coms.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import study.coms.exchange.order.*;
import study.coms.server.exception.WrongIdentifierException;
import study.coms.server.service.OrderService;

import java.util.List;

@RestController
public class OrderController {
    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/user/orders", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public List<XOrderDto> getOrders() {
        return orderService.orders();
    }

    @RequestMapping(value = "/user/bucket", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public List<XOrderDto> getUserOrders() {
        return orderService.userOrders();
    }

    @RequestMapping(value = "/user/orders/add", method = RequestMethod.POST, consumes = "application/json")
    public void addOrder(@RequestBody XOrderDto orderDto) {
        orderService.addOrder(orderDto);
    }

    @RequestMapping(value = "/user/bucket/add", method = RequestMethod.POST, consumes = "application/json")
    public void addUserOrder(@RequestBody XOrderDto orderDto) {
        orderService.addUserOrder(orderDto);
    }

    @RequestMapping(value = "/user/orders/edit", method = RequestMethod.POST, consumes = "application/json")
    public void editOrder(@RequestBody XOrderDto orderDto) {
        orderService.editOrder(orderDto);
    }

    @RequestMapping(value = "/user/bucket/edit", method = RequestMethod.POST, consumes = "application/json")
    public void editUserOrder(@RequestBody XOrderDto orderDto) {
        orderService.editUserOrder(orderDto);
    }

    @RequestMapping(value = "/user/orders/remove", method = RequestMethod.POST, consumes = "application/json")
    public void removeOrder(@RequestBody XOrderDto orderDto) {
        if (orderDto.getN() == null)
            throw new WrongIdentifierException();
        orderService.removeOrder(orderDto.getN());
    }

    @RequestMapping(value = "/user/orders/reserve", method = RequestMethod.POST, consumes = "application/json")
    public void reserveOrder(@RequestBody XOrderDto orderDto) {
        if (orderDto.getN() == null)
            throw new WrongIdentifierException();
        orderService.reserveStock(orderDto.getN());
    }

    @RequestMapping(value = "/user/orders/out", method = RequestMethod.POST, consumes = "application/json")
    public void outOrder(@RequestBody XOrderDto orderDto) {
        if (orderDto.getN() == null)
            throw new WrongIdentifierException();
        orderService.outGoods(orderDto.getN());
    }

    @RequestMapping(value = "/user/orders/unreserve", method = RequestMethod.POST, consumes = "application/json")
    public void unreserveOrder(@RequestBody XOrderDto orderDto) {
        if (orderDto.getN() == null)
            throw new WrongIdentifierException();
        orderService.unreserveStock(orderDto.getN());
    }
}
