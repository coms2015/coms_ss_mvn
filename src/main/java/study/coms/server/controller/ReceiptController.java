package study.coms.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import study.coms.exchange.receipt.XReceiptDto;
import study.coms.server.exception.WrongIdentifierException;
import study.coms.server.service.ReceiptService;

import java.util.List;

@RestController
public class ReceiptController {
    @Autowired
    private ReceiptService receiptService;

    @RequestMapping(value = "/user/receipts", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public List<XReceiptDto> getReceipts() {
        return receiptService.receipts();
    }

    @RequestMapping(value = "/user/receipts/add", method = RequestMethod.POST, consumes = "application/json")
    public void addReceipt(@RequestBody XReceiptDto receiptDto) {
        receiptService.addReceipt(receiptDto);
    }

    @RequestMapping(value = "/user/receipts/edit", method = RequestMethod.POST, consumes = "application/json")
    public void editReceipt(@RequestBody XReceiptDto receiptDto) {
        receiptService.editReceipt(receiptDto);
    }

    @RequestMapping(value = "/user/receipts/remove", method = RequestMethod.POST, consumes = "application/json")
    public void removeReceipt(@RequestBody XReceiptDto receiptDto) {
        if (receiptDto.getN() == null)
            throw new WrongIdentifierException();
        receiptService.removeReceipt(receiptDto.getN());
    }

    @RequestMapping(value = "/user/receipts/in", method = RequestMethod.POST, consumes = "application/json")
    public void toStock(@RequestBody XReceiptDto receiptDto) {
        if (receiptDto.getN() == null)
            throw new WrongIdentifierException();
        receiptService.putToStock(receiptDto.getN());
    }
}
