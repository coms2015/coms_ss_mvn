package study.coms.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import study.coms.exchange.store.XStoreDto;
import study.coms.server.service.StoreService;

import java.util.List;

@RestController
public class StoreController {
    @Autowired
    private StoreService storeService;

    @RequestMapping(value = "/user/stores", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public List<XStoreDto> getStores() throws InterruptedException {
        return storeService.stores();
    }

    @RequestMapping(value = "/user/stores/add", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public void addStore(@RequestBody XStoreDto storeDto) {
        storeService.addStore(storeDto);
    }

    @RequestMapping(value = "/user/stores/edit", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public void editStore(@RequestBody XStoreDto storeDto) {
        storeService.editStore(storeDto);
    }

    @RequestMapping(value = "/user/stores/remove", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public void removeStore(@RequestBody XStoreDto storeDto) {
        storeService.removeStore(storeDto.getN());
    }
}
