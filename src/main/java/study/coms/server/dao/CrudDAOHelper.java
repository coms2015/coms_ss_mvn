package study.coms.server.dao;

import org.hibernate.SessionFactory;
import study.coms.server.entity.IE;

import java.util.List;

@SuppressWarnings({"unchecked", "unused"})
public class CrudDAOHelper<E extends IE> {
    protected SessionFactory sessionFactory;
    protected Class<?> clazz;

    public CrudDAOHelper(SessionFactory sessionFactory, Class<?> clazz) {
        this.sessionFactory = sessionFactory;
        this.clazz = clazz;
    }

    public List<E> get() {
        return sessionFactory.getCurrentSession().createCriteria(clazz).list();
    }

    public void insert(E entity) {
        sessionFactory.getCurrentSession().save(entity);
    }

    public void update(E entity) {
        sessionFactory.getCurrentSession().update(entity);
    }

    public void persist(E entity) {
        sessionFactory.getCurrentSession().persist(entity);
    }

    public E merge(E entity) {
        return (E) sessionFactory.getCurrentSession().merge(entity);
    }

    public void saveOrUpdate(E entity) {
        sessionFactory.getCurrentSession().saveOrUpdate(entity);
    }

    public void delete(E entity) {
        sessionFactory.getCurrentSession().delete(entity);
    }

    public E getById(int id) {
        return (E) sessionFactory.getCurrentSession().get(clazz, id);
    }

    public void deleteById(int id) {
        E obj = (E) sessionFactory.getCurrentSession().get(clazz, id);
        if (obj != null)
            sessionFactory.getCurrentSession().delete(obj);
    }
}