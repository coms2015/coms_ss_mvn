package study.coms.server.dao;

import study.coms.server.entity.EError;

public interface ErrorDAO {
    EError getError(Class<?> exceptionClass);
}
