package study.coms.server.dao;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import study.coms.server.entity.EError;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ErrorDAOImpl implements ErrorDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public EError getError(Class<?> exceptionClass) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(EError.class);
        return (EError) criteria.add(Restrictions.eq("classname", exceptionClass.getName())).uniqueResult();
    }
}
