package study.coms.server.dao;

import study.coms.server.entity.EGoods;
import study.coms.server.entity.EGoodsBarcode;

public interface GoodsBarcodeDAO {
    CrudDAOHelper<EGoodsBarcode> getCrudHelper();

    void deleteByGoods(EGoods goods);
}
