package study.coms.server.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import study.coms.server.entity.EGoods;
import study.coms.server.entity.EGoodsBarcode;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class GoodsBarcodeDAOImpl implements GoodsBarcodeDAO {
    @Autowired
    private SessionFactory sessionFactory;

    private CrudDAOHelper<EGoodsBarcode> crudHelper = null;

    @Override
    public CrudDAOHelper<EGoodsBarcode> getCrudHelper() {
        return crudHelper == null ? crudHelper = new CrudDAOHelper<>(sessionFactory, EGoodsBarcode.class) : crudHelper;
    }

    @Override
    public void deleteByGoods(EGoods goods) {
        sessionFactory.getCurrentSession().createQuery("delete from EGoodsBarcode where goods = :goods").setParameter("goods", goods).executeUpdate();
    }
}
