package study.coms.server.dao;

import study.coms.server.entity.EGoods;
import study.coms.server.entity.EStore;

import java.util.List;

public interface GoodsDAO {
    CrudDAOHelper<EGoods> getCrudHelper();
    @SuppressWarnings("unchecked")
    List<EGoods> goodsByStore(EStore store);
}
