package study.coms.server.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import study.coms.server.entity.EGoods;
import study.coms.server.entity.EStore;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class GoodsDAOImpl implements GoodsDAO {
    @Autowired
    private SessionFactory sessionFactory;

    private CrudDAOHelper<EGoods> crudHelper = null;

    @Override
    public CrudDAOHelper<EGoods> getCrudHelper() {
        return crudHelper == null ? crudHelper = new CrudDAOHelper<>(sessionFactory, EGoods.class) : crudHelper;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EGoods> goodsByStore(EStore store) {
        return sessionFactory.getCurrentSession().createQuery("select g from EGoods g join g.stores s where s.store = :store")
                .setParameter("store", store).list();
    }
}
