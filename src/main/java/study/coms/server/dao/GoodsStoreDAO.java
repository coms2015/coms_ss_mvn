package study.coms.server.dao;

import study.coms.server.entity.EGoods;
import study.coms.server.entity.EGoodsStore;

public interface GoodsStoreDAO {
    CrudDAOHelper<EGoodsStore> getCrudHelper();

    void deleteByGoods(EGoods goods);
}
