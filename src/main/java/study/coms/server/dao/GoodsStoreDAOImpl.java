package study.coms.server.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import study.coms.server.entity.EGoods;
import study.coms.server.entity.EGoodsStore;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class GoodsStoreDAOImpl implements GoodsStoreDAO {
    @Autowired
    private SessionFactory sessionFactory;

    private CrudDAOHelper<EGoodsStore> crudHelper = null;

    @Override
    public CrudDAOHelper<EGoodsStore> getCrudHelper() {
        return crudHelper == null ? crudHelper = new CrudDAOHelper<>(sessionFactory, EGoodsStore.class) : crudHelper;
    }

    @Override
    public void deleteByGoods(EGoods goods) {
        sessionFactory.getCurrentSession().createQuery("delete from EGoodsStore where goods = :goods").setParameter("goods", goods).executeUpdate();
    }
}
