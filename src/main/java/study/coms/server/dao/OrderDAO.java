package study.coms.server.dao;

import study.coms.server.entity.EOrder;
import study.coms.server.entity.EUser;

import java.util.List;

public interface OrderDAO {
    CrudDAOHelper<EOrder> getCrudHelper();

    @SuppressWarnings("unchecked")
    List<EOrder> getOrdersByUser(EUser user);
}
