package study.coms.server.dao;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import study.coms.server.entity.EOrder;
import study.coms.server.entity.EUser;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class OrderDAOImpl implements OrderDAO {
    @Autowired
    private SessionFactory sessionFactory;

    private CrudDAOHelper<EOrder> crudHelper = null;

    @Override
    public CrudDAOHelper<EOrder> getCrudHelper() {
        return crudHelper == null ? crudHelper = new CrudDAOHelper<>(sessionFactory, EOrder.class) : crudHelper;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<EOrder> getOrdersByUser(EUser user) {
        return sessionFactory.getCurrentSession().createCriteria(EOrder.class).add(Restrictions.eq("user", user)).list();
    }
}
