package study.coms.server.dao;

import study.coms.server.entity.EReceipt;

public interface ReceiptDAO {
    CrudDAOHelper<EReceipt> getCrudHelper();
}
