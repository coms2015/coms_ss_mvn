package study.coms.server.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import study.coms.server.entity.EReceipt;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class ReceiptDAOImpl implements ReceiptDAO {
    @Autowired
    private SessionFactory sessionFactory;

    private CrudDAOHelper<EReceipt> crudHelper = null;

    @Override
    public CrudDAOHelper<EReceipt> getCrudHelper() {
        return crudHelper == null ? crudHelper = new CrudDAOHelper<>(sessionFactory, EReceipt.class) : crudHelper;
    }
}
