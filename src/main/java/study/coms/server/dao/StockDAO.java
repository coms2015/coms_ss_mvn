package study.coms.server.dao;

import study.coms.server.entity.EGoods;
import study.coms.server.entity.EStock;
import study.coms.server.entity.EStore;

import java.util.List;

public interface StockDAO {
    CrudDAOHelper<EStock> getCrudHelper();

    @SuppressWarnings("unchecked")
    void addStock(EStock stock);

    @SuppressWarnings("unchecked")
    List<EStock> getForReserve(EStore store, EGoods goods, int amount);
}
