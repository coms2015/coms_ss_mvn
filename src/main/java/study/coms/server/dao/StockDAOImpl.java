package study.coms.server.dao;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import study.coms.server.entity.EGoods;
import study.coms.server.entity.EStock;
import study.coms.server.entity.EStore;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class StockDAOImpl implements StockDAO {
    @Autowired
    private SessionFactory sessionFactory;

    private CrudDAOHelper<EStock> crudHelper = null;

    @Override
    public CrudDAOHelper<EStock> getCrudHelper() {
        return crudHelper == null ? crudHelper = new CrudDAOHelper<>(sessionFactory, EStock.class) : crudHelper;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void addStock(EStock stock) {
        if (stock.getInOrder() == 1)
            getCrudHelper().insert(stock);
        else {
            List<EStock> currentStock = sessionFactory.getCurrentSession().createCriteria(EStock.class).
                    add(Restrictions.eq("goods", stock.getGoods())).add(Restrictions.eq("store", stock.getStore())).
                    add(Restrictions.eq("inOrder", 0)).list();
            if (currentStock.size() > 0) {
                EStock addStock = currentStock.get(0);
                addStock.setAmount(addStock.getAmount() + stock.getAmount());
                crudHelper.update(addStock);
            } else {
                crudHelper.insert(stock);
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<EStock> getForReserve(EStore store, EGoods goods, int amount) {
        return sessionFactory.getCurrentSession().createCriteria(EStock.class)
                .add(Restrictions.eq("store", store)).add(Restrictions.eq("goods", goods)).add(Restrictions.eq("inOrder", 0))
                .add(Restrictions.ge("amount", amount)).list();
    }
}
