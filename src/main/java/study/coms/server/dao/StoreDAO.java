package study.coms.server.dao;

import study.coms.server.entity.EStore;

public interface StoreDAO {
    CrudDAOHelper<EStore> getCrudHelper();
}
