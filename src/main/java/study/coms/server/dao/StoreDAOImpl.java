package study.coms.server.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import study.coms.server.entity.EStore;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class StoreDAOImpl implements StoreDAO {
    @Autowired
    private SessionFactory sessionFactory;

    private CrudDAOHelper<EStore> crudHelper = null;

    @Override
    public CrudDAOHelper<EStore> getCrudHelper() {
        return crudHelper == null ? crudHelper = new CrudDAOHelper<>(sessionFactory, EStore.class) : crudHelper;
    }
}
