package study.coms.server.dao;

import study.coms.server.entity.EUser;

public interface UserDAO {
    EUser getUserByLoginAndPassword(String login, String password);
}
