package study.coms.server.dao;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import study.coms.server.entity.EUser;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class UserDAOImpl implements UserDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public EUser getUserByLoginAndPassword(String login, String password) {
        return (EUser) sessionFactory.getCurrentSession().createCriteria(EUser.class).add(Restrictions.eq("login", login))
                .add(Restrictions.eq("password", password)).uniqueResult();
    }
}
