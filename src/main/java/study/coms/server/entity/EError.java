package study.coms.server.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "errors", catalog = "coms")
public class EError implements IE {
    private Integer code;
    private String name;
    private String comment;
    private Integer httpcode;
    private String classname;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "code", nullable = false, insertable = true, updatable = false)
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Column(name = "name", nullable = false, insertable = true, updatable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "comment", nullable = true, insertable = true, updatable = false, length = 255)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Column(name = "httpcode", nullable = true, insertable = true, updatable = false)
    public Integer getHttpcode() {
        return httpcode;
    }

    public void setHttpcode(Integer httpcode) {
        this.httpcode = httpcode;
    }

    @Column(name = "classname", nullable = true, insertable = true, updatable = false, length = 100)
    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EError eError = (EError) o;
        return Objects.equals(code, eError.code) &&
                Objects.equals(httpcode, eError.httpcode) &&
                Objects.equals(name, eError.name) &&
                Objects.equals(comment, eError.comment) &&
                Objects.equals(classname, eError.classname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name, comment, httpcode, classname);
    }
}
