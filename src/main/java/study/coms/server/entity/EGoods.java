package study.coms.server.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "goods", catalog = "coms")
public class EGoods implements IE {
    private Integer n;
    private String code;
    private String name;
    private List<EGoodsBarcode> barcodes  = new ArrayList<>(0);
    private List<EGoodsStore> stores = new ArrayList<>(0);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "n", insertable = true, updatable = false, nullable = false)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @Column(name = "code", insertable = true, updatable = true, nullable = false, length = 50, unique = true)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "name", insertable = true, updatable = true, nullable = false, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "goods", orphanRemoval = true, cascade = CascadeType.ALL)
    public List<EGoodsBarcode> getBarcodes() {
        return barcodes;
    }

    public void setBarcodes(List<EGoodsBarcode> barcodes) {
        this.barcodes = barcodes;
    }

    @OneToMany(mappedBy = "goods", orphanRemoval = true, cascade = CascadeType.ALL)
    public List<EGoodsStore> getStores() {
        return stores;
    }

    public void setStores(List<EGoodsStore> stores) {
        this.stores = stores;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EGoods eGoods = (EGoods) o;
        return Objects.equals(n, eGoods.n) &&
                Objects.equals(code, eGoods.code) &&
                Objects.equals(name, eGoods.name) &&
                Objects.equals(barcodes, eGoods.barcodes) &&
                Objects.equals(stores, eGoods.stores);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, code, name, barcodes, stores);
    }
}
