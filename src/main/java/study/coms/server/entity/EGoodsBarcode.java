package study.coms.server.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "goods_barcodes", catalog = "coms")
public class EGoodsBarcode implements IE {
    private Integer n;
    private String barcode;
    private EGoods goods;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "n", insertable = true, updatable = false, nullable = false)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @Column(name = "barcode", insertable = true, updatable = true, nullable = false, unique = true)
    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    @ManyToOne
    @JoinColumn(name = "goods_n", referencedColumnName = "n", nullable = false)
    public EGoods getGoods() {
        return goods;
    }

    public void setGoods(EGoods goods) {
        this.goods = goods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EGoodsBarcode that = (EGoodsBarcode) o;
        return Objects.equals(n, that.n) &&
                Objects.equals(barcode, that.barcode) &&
                Objects.equals(goods, that.goods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, barcode, goods);
    }
}
