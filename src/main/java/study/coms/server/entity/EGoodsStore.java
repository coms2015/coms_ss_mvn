package study.coms.server.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "goods_stores", catalog = "coms")
public class EGoodsStore implements IE {
    private Integer n;
    private EGoods goods;
    private EStore store;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "n", insertable = true, updatable = false, nullable = false)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @ManyToOne
    @JoinColumn(name = "goods_n", referencedColumnName = "n", nullable = false)
    public EGoods getGoods() {
        return goods;
    }

    public void setGoods(EGoods goods) {
        this.goods = goods;
    }

    @ManyToOne
    @JoinColumn(name = "stores_n", referencedColumnName = "n", nullable = false)
    public EStore getStore() {
        return store;
    }

    public void setStore(EStore store) {
        this.store = store;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EGoodsStore that = (EGoodsStore) o;
        return Objects.equals(n, that.n) &&
                Objects.equals(goods, that.goods) &&
                Objects.equals(store, that.store);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, goods, store);
    }
}
