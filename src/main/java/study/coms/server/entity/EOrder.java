package study.coms.server.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "orders", catalog = "coms")
public class EOrder implements IE {
    private Integer n;
    private EStore store;
    private String code;
    private EUser user;
    private Date createdAt;
    private Date executedAt;
    private Integer executed;
    private List<EOrderGoods> goods = new ArrayList<>(0);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "n", insertable = true, updatable = false, nullable = false)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @ManyToOne
    @JoinColumn(name = "stores_n", referencedColumnName = "n", nullable = false)
    public EStore getStore() {
        return store;
    }

    public void setStore(EStore store) {
        this.store = store;
    }

    @Column(name = "code", insertable = true, updatable = false, nullable = false, length = 45, unique = true)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @ManyToOne
    @JoinColumn(name = "users_n", referencedColumnName = "n", nullable = false)
    public EUser getUser() {
        return user;
    }

    public void setUser(EUser user) {
        this.user = user;
    }

    @Column(name = "created_at", insertable = true, updatable = true, nullable = false)
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Column(name = "execute_at", insertable = true, updatable = true, nullable = true)
    public Date getExecutedAt() {
        return executedAt;
    }

    public void setExecutedAt(Date executedAt) {
        this.executedAt = executedAt;
    }

    @Column(name = "executed", insertable = true, updatable = true, nullable = false)
    public Integer getExecuted() {
        return executed;
    }

    public void setExecuted(Integer executed) {
        this.executed = executed;
    }

    @OneToMany(mappedBy = "order", orphanRemoval = true, cascade = CascadeType.ALL)
    public List<EOrderGoods> getGoods() {
        return goods;
    }

    public void setGoods(List<EOrderGoods> goods) {
        this.goods = goods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EOrder eOrder = (EOrder) o;
        return Objects.equals(n, eOrder.n) &&
                Objects.equals(store, eOrder.store) &&
                Objects.equals(code, eOrder.code) &&
                Objects.equals(user, eOrder.user) &&
                Objects.equals(createdAt, eOrder.createdAt) &&
                Objects.equals(executedAt, eOrder.executedAt) &&
                Objects.equals(executed, eOrder.executed) &&
                Objects.equals(goods, eOrder.goods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, store, code, user, createdAt, executedAt, executed, goods);
    }
}
