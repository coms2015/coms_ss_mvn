package study.coms.server.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "orders_goods", catalog = "coms")
public class EOrderGoods implements IE {
    private Integer n;
    private EOrder order;
    private EGoods goods;
    private Integer amount;
    private EStock stock;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "n", insertable = true, updatable = false, nullable = false)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @ManyToOne
    @JoinColumn(name = "orders_n", referencedColumnName = "n", nullable = false)
    public EOrder getOrder() {
        return order;
    }

    public void setOrder(EOrder order) {
        this.order = order;
    }

    @ManyToOne
    @JoinColumn(name = "goods_n", referencedColumnName = "n", nullable = false)
    public EGoods getGoods() {
        return goods;
    }

    public void setGoods(EGoods goods) {
        this.goods = goods;
    }

    @Column(name = "amount", insertable = true, updatable = true, nullable = false)
    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @ManyToOne
    @JoinColumn(name = "stock_n", referencedColumnName = "n", nullable = true)
    public EStock getStock() {
        return stock;
    }

    public void setStock(EStock stock) {
        this.stock = stock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EOrderGoods that = (EOrderGoods) o;
        return Objects.equals(n, that.n) &&
                Objects.equals(order, that.order) &&
                Objects.equals(goods, that.goods) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(stock, that.stock);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, order, goods, amount, stock);
    }
}
