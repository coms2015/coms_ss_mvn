package study.coms.server.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "receipts", catalog = "coms")
public class EReceipt implements IE {
    private Integer n;
    private EStore store;
    private String code;
    private Date createdAt;
    private Date arrivalAt;
    private Integer arrived;
    private List<EReceiptGoods> goods = new ArrayList<>(0);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "n", insertable = true, updatable = false, nullable = false)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @ManyToOne
    @JoinColumn(name = "stores_n", referencedColumnName = "n", nullable = false)
    public EStore getStore() {
        return store;
    }

    public void setStore(EStore store) {
        this.store = store;
    }

    @Column(name = "code", insertable = true, updatable = false, nullable = false, length = 45, unique = true)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "created_at", insertable = true, updatable = true, nullable = false)
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Column(name = "arrival_at", insertable = true, updatable = true, nullable = false)
    public Date getArrivalAt() {
        return arrivalAt;
    }

    public void setArrivalAt(Date arrivalAt) {
        this.arrivalAt = arrivalAt;
    }

    @Column(name = "arrived", insertable = true, updatable = true, nullable = false)
    public Integer getArrived() {
        return arrived;
    }

    public void setArrived(Integer arrived) {
        this.arrived = arrived;
    }

    @OneToMany(mappedBy = "receipt", orphanRemoval = true, cascade = CascadeType.ALL)
    public List<EReceiptGoods> getGoods() {
        return goods;
    }

    public void setGoods(List<EReceiptGoods> goods) {
        this.goods = goods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EReceipt eReceipt = (EReceipt) o;
        return Objects.equals(n, eReceipt.n) &&
                Objects.equals(store, eReceipt.store) &&
                Objects.equals(code, eReceipt.code) &&
                Objects.equals(createdAt, eReceipt.createdAt) &&
                Objects.equals(arrivalAt, eReceipt.arrivalAt) &&
                Objects.equals(arrived, eReceipt.arrived) &&
                Objects.equals(goods, eReceipt.goods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, store, code, createdAt, arrivalAt, arrived, goods);
    }
}
