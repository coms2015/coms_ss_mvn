package study.coms.server.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "receipts_goods", catalog = "coms")
public class EReceiptGoods implements IE {
    private Integer n;
    private Integer amount;
    private EReceipt receipt;
    private EGoods goods;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "n", insertable = true, updatable = false, nullable = false)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @Column(name = "amount", insertable = true, updatable = true, nullable = false)
    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @ManyToOne
    @JoinColumn(name = "receipts_n", referencedColumnName = "n", nullable = false)
    public EReceipt getReceipt() {
        return receipt;
    }

    public void setReceipt(EReceipt receipt) {
        this.receipt = receipt;
    }

    @ManyToOne
    @JoinColumn(name = "goods_n", referencedColumnName = "n", nullable = false)
    public EGoods getGoods() {
        return goods;
    }

    public void setGoods(EGoods goods) {
        this.goods = goods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EReceiptGoods that = (EReceiptGoods) o;
        return Objects.equals(n, that.n) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(receipt, that.receipt) &&
                Objects.equals(goods, that.goods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, amount, receipt, goods);
    }
}
