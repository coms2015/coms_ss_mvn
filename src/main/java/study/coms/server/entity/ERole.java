package study.coms.server.entity;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "roles", catalog = "coms")
public class ERole implements IE {
    private Integer n;
    private String name;
    private String modules;
    private Collection<EUser> users;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "n", insertable = true, updatable = false, nullable = false)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @Column(name = "name", insertable = true, updatable = true, nullable = false, length = 45, unique = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "modules", insertable = true, updatable = true, nullable = true, length = 500)
    public String getModules() {
        return modules;
    }

    public void setModules(String modules) {
        this.modules = modules;
    }

    @OneToMany(mappedBy = "role")
    public Collection<EUser> getUsers() {
        return users;
    }

    public void setUsers(Collection<EUser> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ERole eRole = (ERole) o;
        return Objects.equals(n, eRole.n) &&
                Objects.equals(name, eRole.name) &&
                Objects.equals(modules, eRole.modules) &&
                Objects.equals(users, eRole.users);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, name, modules, users);
    }
}
