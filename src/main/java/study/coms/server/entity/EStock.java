package study.coms.server.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "stock", catalog = "coms")
public class EStock implements IE {
    private Integer n;
    private EStore store;
    private Integer amount;
    private Integer inOrder;
    private EGoods goods;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "n", insertable = true, updatable = false, nullable = false)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @ManyToOne
    @JoinColumn(name = "stores_n", referencedColumnName = "n", nullable = false)
    public EStore getStore() {
        return store;
    }

    public void setStore(EStore store) {
        this.store = store;
    }

    @Column(name = "amount", insertable = true, updatable = true, nullable = false)
    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Column(name = "in_order", insertable = true, updatable = true, nullable = false)
    public Integer getInOrder() {
        return inOrder;
    }

    public void setInOrder(Integer inOrder) {
        this.inOrder = inOrder;
    }

    @ManyToOne
    @JoinColumn(name = "goods_n", referencedColumnName = "n", nullable = false)
    public EGoods getGoods() {
        return goods;
    }

    public void setGoods(EGoods goods) {
        this.goods = goods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EStock eStock = (EStock) o;
        return Objects.equals(n, eStock.n) &&
                Objects.equals(store, eStock.store) &&
                Objects.equals(amount, eStock.amount) &&
                Objects.equals(inOrder, eStock.inOrder) &&
                Objects.equals(goods, eStock.goods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, store, amount, inOrder, goods);
    }
}
