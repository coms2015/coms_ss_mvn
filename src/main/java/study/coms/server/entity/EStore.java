package study.coms.server.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "stores", catalog = "coms")
public class EStore implements IE {
    private Integer n;
    private String name;
    private String address;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "n", insertable = true, updatable = false, nullable = false)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @Column(name = "name", insertable = true, updatable = true, nullable = false, length = 100, unique = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "address", insertable = true, updatable = true, nullable = false)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EStore eStore = (EStore) o;
        return Objects.equals(n, eStore.n) &&
                Objects.equals(name, eStore.name) &&
                Objects.equals(address, eStore.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, name, address);
    }
}
