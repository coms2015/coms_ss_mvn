package study.coms.server.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "users", catalog = "coms")
public class EUser implements IE {
    private Integer n;
    private String login;
    private String mail;
    private String password;
    private ERole role;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "n", insertable = true, updatable = false, nullable = false)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @Column(name = "login", insertable = true, updatable = false, nullable = false, length = 45, unique = true)
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Column(name = "mail", insertable = true, updatable = true, nullable = true, length = 100)
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Column(name = "password", insertable = true, updatable = true, nullable = false, length = 40)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @ManyToOne
    @JoinColumn(name = "roles_n", referencedColumnName = "n", nullable = false)
    public ERole getRole() {
        return role;
    }

    public void setRole(ERole role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EUser eUser = (EUser) o;
        return Objects.equals(n, eUser.n) &&
                Objects.equals(login, eUser.login) &&
                Objects.equals(mail, eUser.mail) &&
                Objects.equals(password, eUser.password) &&
                Objects.equals(role, eUser.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, login, mail, password, role);
    }
}
