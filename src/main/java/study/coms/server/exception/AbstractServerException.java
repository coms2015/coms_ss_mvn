package study.coms.server.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractServerException extends RuntimeException {
    private static final Logger logger = LoggerFactory.getLogger(AbstractServerException.class);

    public AbstractServerException(Throwable cause) {
        super(cause);
        logger.error(cause.getMessage(), cause);
    }

    public AbstractServerException() {
        logger.error(null, this);
    }

    public AbstractServerException(String message) {
        super(message);
        logger.error(message, this);
    }

    public AbstractServerException(String message, Throwable cause) {
        super(message, cause);
        logger.error(message, cause);
    }
}
