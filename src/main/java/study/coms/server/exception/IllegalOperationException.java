package study.coms.server.exception;

public final class IllegalOperationException extends AbstractServerException {
    public IllegalOperationException() {
        super();
    }
}
