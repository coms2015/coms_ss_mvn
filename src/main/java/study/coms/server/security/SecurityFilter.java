package study.coms.server.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import study.coms.exchange.message.XErrorResp;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public final class SecurityFilter implements Filter {
    private static final Logger logger = LoggerFactory.getLogger(SecurityFilter.class);

    @Autowired
    private UserSession session;

    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        logger.info(String.format("New request to %s", ((HttpServletRequest) servletRequest).getRequestURI()));
        if (session.isValid()) {
            if (!session.hasPermission(((HttpServletRequest) servletRequest).getRequestURI())) {
                logger.warn(String.format("Forbidden %s%s", ((HttpServletRequest) servletRequest).getRequestURI(), session.getUser().getLogin()));
                ((HttpServletResponse) servletResponse).setStatus(403); //FORBIDDEN
                return;
            }
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        logger.warn(String.format("Unauthorized %s", servletRequest.getRemoteHost()));
        ((HttpServletResponse) servletResponse).setStatus(401); //UNAUTHORIZED
        servletResponse.getWriter().print(mapper.writeValueAsString(new XErrorResp(1, "Authorization Error")));
    }

    @Override
    public void destroy() {}
}
