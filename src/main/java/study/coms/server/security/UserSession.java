package study.coms.server.security;

import study.coms.server.entity.EUser;

public interface UserSession {
    void init(EUser user);

    boolean isValid();

    Boolean hasPermission(String uri);

    EUser getUser();

    void reset();
}
