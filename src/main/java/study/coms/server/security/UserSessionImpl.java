package study.coms.server.security;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import study.coms.server.entity.EUser;

import java.util.Date;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserSessionImpl implements UserSession {
    private EUser user = null;
    private long lastAction;
    private static final long expire = 6000000;
    private String[] modules;

    @Override
    public void init(EUser user) {
        this.user = user;
        lastAction = new Date().getTime();
        modules = user.getRole().getModules().split(",");
    }

    @Override
    public boolean isValid() {
        long now = new Date().getTime();
        if (now < expire + lastAction && user != null) {
            lastAction = now;
            return true;
        }
        return false;
    }

    @Override
    public Boolean hasPermission(String uri) {
        if (user == null)
            return false;
        for (String module : modules) {
            if (uri.contains("/" + module))
                return true;
        }
        return false;
    }

    @Override
    public EUser getUser() {
        return user;
    }

    @Override
    public void reset() {
        user = null;
    }
}
