package study.coms.server.service;

import org.springframework.transaction.annotation.Transactional;
import study.coms.server.entity.EError;

public interface ErrorService {
    @Transactional
    EError getErrorByClass(Class<?> clazz);
}
