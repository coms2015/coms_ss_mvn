package study.coms.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import study.coms.server.dao.ErrorDAO;
import study.coms.server.entity.EError;

@Service
public class ErrorServiceImpl implements ErrorService {
    @Autowired
    private ErrorDAO errorDAO;

    @Override
    public EError getErrorByClass(Class<?> clazz) {
        return errorDAO.getError(clazz);
    }
}
