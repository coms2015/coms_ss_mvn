package study.coms.server.service;

import org.springframework.transaction.annotation.Transactional;
import study.coms.exchange.goods.XGoodsDto;

import java.util.List;

public interface GoodsService {
    List<XGoodsDto> goods();

    @Transactional
    List<XGoodsDto> goodsByStoreN(int n);

    void addGoods(XGoodsDto goodsDto);

    void editGoods(XGoodsDto goodsDto);

    void removeGoods(int n);
}
