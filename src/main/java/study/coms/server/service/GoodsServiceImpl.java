package study.coms.server.service;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import study.coms.exchange.goods.XGoodsDto;
import study.coms.server.dao.GoodsDAO;
import study.coms.server.dao.StoreDAO;
import study.coms.server.entity.EGoods;
import study.coms.server.entity.EStore;

import java.util.LinkedList;
import java.util.List;

@Service
public class GoodsServiceImpl implements GoodsService {
    @Autowired
    private GoodsDAO goodsDAO;

    @Autowired
    private StoreDAO storeDAO;

    @Autowired
    private Mapper mapper;

    @Transactional
    @Override
    public List<XGoodsDto> goods() {
        List<EGoods> eGoods = goodsDAO.getCrudHelper().get();
        List<XGoodsDto> dtoGoods = new LinkedList<>();
        eGoods.forEach(g -> dtoGoods.add(mapper.map(g, XGoodsDto.class)));
        return dtoGoods;
    }

    @Transactional
    @Override
    public List<XGoodsDto> goodsByStoreN(int n) {
        EStore store = storeDAO.getCrudHelper().getById(n);
        List<EGoods> eGoods = goodsDAO.goodsByStore(store);
        List<XGoodsDto> dtoGoods = new LinkedList<>();
        eGoods.forEach(g -> dtoGoods.add(mapper.map(g, XGoodsDto.class)));
        return dtoGoods;
    }

    @Transactional
    @Override
    public void addGoods(XGoodsDto goodsDto) {
        goodsDAO.getCrudHelper().insert(mapper.map(goodsDto, EGoods.class));
    }

    @Transactional
    @Override
    public void editGoods(XGoodsDto goodsDto) {
        goodsDAO.getCrudHelper().merge(mapper.map(goodsDto, EGoods.class));
    }

    @Transactional
    @Override
    public void removeGoods(int n) {
        goodsDAO.getCrudHelper().deleteById(n);
    }
}
