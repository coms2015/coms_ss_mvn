package study.coms.server.service;

import study.coms.exchange.order.XOrderDto;

import java.util.List;

public interface OrderService {
    List<XOrderDto> orders();

    List<XOrderDto> userOrders();

    void addOrder(XOrderDto orderDto);

    void addUserOrder(XOrderDto orderDto);

    void editOrder(XOrderDto orderDto);

    void editUserOrder(XOrderDto orderDto);

    void removeOrder(int orderN);

    void reserveStock(int orderN);

    void unreserveStock(int orderN);

    void outGoods(int orderN);
}
