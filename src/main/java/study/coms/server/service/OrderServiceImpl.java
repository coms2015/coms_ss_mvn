package study.coms.server.service;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import study.coms.exchange.order.XOrderDto;
import study.coms.server.dao.OrderDAO;
import study.coms.server.dao.StockDAO;
import study.coms.server.entity.EOrder;
import study.coms.server.entity.EOrderGoods;
import study.coms.server.entity.EStock;
import study.coms.server.exception.IllegalOperationException;
import study.coms.server.exception.ReserveDeficiencyException;
import study.coms.server.security.UserSession;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private StockDAO stockDAO;

    @Autowired
    private UserSession userSession;

    @Autowired
    private Mapper mapper;

    @Transactional
    @Override
    public List<XOrderDto> orders() {
        List<EOrder> eOrders = orderDAO.getCrudHelper().get();
        List<XOrderDto> dtoOrders = new LinkedList<>();
        eOrders.forEach(eOrder -> dtoOrders.add(mapper.map(eOrder, XOrderDto.class)));
        return dtoOrders;
    }

    @Transactional
    @Override
    public List<XOrderDto> userOrders() {
        List<EOrder> eOrders = orderDAO.getOrdersByUser(userSession.getUser());
        List<XOrderDto> dtoOrders = new LinkedList<>();
        eOrders.forEach(eOrder -> dtoOrders.add(mapper.map(eOrder, XOrderDto.class)));
        return dtoOrders;
    }

    @Transactional
    @Override
    public void addOrder(XOrderDto orderDto) {
        orderDAO.getCrudHelper().insert(mapper.map(orderDto, EOrder.class));
    }

    @Transactional
    @Override
    public void addUserOrder(XOrderDto orderDto) {
        EOrder order = mapper.map(orderDto, EOrder.class);
        if (!Objects.equals(order.getUser().getN(), userSession.getUser().getN()))
            throw new IllegalOperationException();
    }

    @Transactional
    @Override
    public void editOrder(XOrderDto orderDto) {
        EOrder order = orderDAO.getCrudHelper().getById(orderDto.getN());
        if (order == null || order.getExecuted() == 1)
            throw new IllegalOperationException();
        orderDAO.getCrudHelper().merge(mapper.map(orderDto, EOrder.class));
    }

    @Transactional
    @Override
    public void editUserOrder(XOrderDto orderDto) {
        EOrder order = orderDAO.getCrudHelper().getById(orderDto.getN());
        if (order == null || order.getExecuted() == 1)
            throw new IllegalOperationException();
        if (!Objects.equals(order.getUser().getN(), userSession.getUser().getN()))
            throw new IllegalOperationException();
        orderDAO.getCrudHelper().merge(mapper.map(orderDto, EOrder.class));
    }

    @Transactional
    @Override
    public void removeOrder(int orderN) {
        try {
            unreserveStock(orderN);
        } catch (IllegalOperationException ignored) {}
        orderDAO.getCrudHelper().deleteById(orderN);
    }

    @Transactional
    @Override
    public void reserveStock(int orderN) {
        EOrder order = orderDAO.getCrudHelper().getById(orderN);
        if (order == null || order.getExecuted() == 1)
            throw new IllegalOperationException();
        for (EOrderGoods orderGoods : order.getGoods()) {
            List<EStock> stock = stockDAO.getForReserve(order.getStore(), orderGoods.getGoods(), orderGoods.getAmount());
            if (stock == null || stock.size() == 0)
                throw new ReserveDeficiencyException();
            EStock selStock = stock.get(0);
            if (selStock.getAmount() > orderGoods.getAmount()) {
                EStock newStock = new EStock();
                newStock.setStore(selStock.getStore());
                newStock.setGoods(selStock.getGoods());
                newStock.setInOrder(0);
                newStock.setAmount(selStock.getAmount() - orderGoods.getAmount());
                stockDAO.getCrudHelper().insert(newStock);
            }
            selStock.setAmount(selStock.getAmount() - orderGoods.getAmount());
            selStock.setInOrder(0);
            orderGoods.setStock(selStock);
            stockDAO.getCrudHelper().update(selStock);
        }
        orderDAO.getCrudHelper().update(order);
    }

    @Transactional
    @Override
    public void unreserveStock(int orderN) {
        EOrder order = orderDAO.getCrudHelper().getById(orderN);
        if (order == null || order.getExecuted() == 0)
            throw new IllegalOperationException();
        for (EOrderGoods orderGoods : order.getGoods()) {
            EStock stock = orderGoods.getStock();
            if (stock != null) {
                EStock newStock = new EStock();
                newStock.setGoods(stock.getGoods());
                newStock.setStore(stock.getStore());
                newStock.setAmount(stock.getAmount());
                newStock.setInOrder(0);
                stockDAO.getCrudHelper().delete(stock);
                stockDAO.addStock(newStock);
            }
        }
    }

    @Transactional
    @Override
    public void outGoods(int orderN) {
        EOrder order = orderDAO.getCrudHelper().getById(orderN);
        order.getGoods().forEach(orderGoods -> {
            EStock stock = orderGoods.getStock();
            if (stock != null)
                stockDAO.getCrudHelper().delete(stock);
        });
        orderDAO.getCrudHelper().delete(order);
    }
}
