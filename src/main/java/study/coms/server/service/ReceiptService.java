package study.coms.server.service;

import org.springframework.transaction.annotation.Transactional;
import study.coms.exchange.receipt.XReceiptDto;

import java.util.List;

public interface ReceiptService {
    @Transactional
    List<XReceiptDto> receipts();

    void addReceipt(XReceiptDto receiptDto);

    void editReceipt(XReceiptDto receiptDto);

    void removeReceipt(int receiptN);

    void putToStock(int receiptN);
}
