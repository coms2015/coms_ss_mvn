package study.coms.server.service;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import study.coms.exchange.receipt.XReceiptDto;
import study.coms.server.dao.ReceiptDAO;
import study.coms.server.dao.StockDAO;
import study.coms.server.entity.EReceipt;
import study.coms.server.entity.EStock;
import study.coms.server.exception.WrongIdentifierException;

import java.util.LinkedList;
import java.util.List;

@Service
public class ReceiptServiceImpl implements ReceiptService {
    @Autowired
    private ReceiptDAO receiptDAO;

    @Autowired
    private StockDAO stockDAO;

    @Autowired
    private Mapper mapper;

    @Transactional
    @Override
    public List<XReceiptDto> receipts() {
        List<EReceipt> eReceipts = receiptDAO.getCrudHelper().get();
        List<XReceiptDto> dtoReceipts = new LinkedList<>();
        eReceipts.forEach(eReceipt -> dtoReceipts.add(mapper.map(eReceipt, XReceiptDto.class)));
        return dtoReceipts;
    }

    @Transactional
    @Override
    public void addReceipt(XReceiptDto receiptDto) {
        receiptDAO.getCrudHelper().insert(mapper.map(receiptDto, EReceipt.class));
    }

    @Transactional
    @Override
    public void editReceipt(XReceiptDto receiptDto) {
        receiptDAO.getCrudHelper().merge(mapper.map(receiptDto, EReceipt.class));
    }

    @Transactional
    @Override
    public void removeReceipt(int receiptN) {
        receiptDAO.getCrudHelper().deleteById(receiptN);
    }

    @Transactional
    @Override
    public void putToStock(int receiptN) {
        EReceipt receipt = receiptDAO.getCrudHelper().getById(receiptN);
        if (receipt == null)
            throw new WrongIdentifierException();
        receipt.getGoods().forEach(goods -> {
            EStock newStock = new EStock();
            newStock.setAmount(goods.getAmount());
            newStock.setGoods(goods.getGoods());
            newStock.setStore(receipt.getStore());
            newStock.setInOrder(0);
            stockDAO.addStock(newStock);
        });
        receiptDAO.getCrudHelper().delete(receipt);
    }
}
