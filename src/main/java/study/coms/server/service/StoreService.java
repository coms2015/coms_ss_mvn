package study.coms.server.service;

import org.springframework.transaction.annotation.Transactional;
import study.coms.exchange.store.XStoreDto;

import java.util.List;

public interface StoreService {
    List<XStoreDto> stores();

    void addStore(XStoreDto storeDto);

    void editStore(XStoreDto storeDto);

    @Transactional
    void removeStore(int n);
}
