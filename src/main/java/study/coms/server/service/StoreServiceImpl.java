package study.coms.server.service;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import study.coms.exchange.store.XStoreDto;
import study.coms.server.dao.StoreDAO;
import study.coms.server.entity.EStore;

import java.util.LinkedList;
import java.util.List;

@Service
public class StoreServiceImpl implements StoreService {
    @Autowired
    private StoreDAO storeDAO;

    @Autowired
    private Mapper mapper;

    @Transactional
    @Override
    public List<XStoreDto> stores() {
        List<EStore> eStores = storeDAO.getCrudHelper().get();
        List<XStoreDto> dtoStores = new LinkedList<>();
        eStores.forEach(eStore -> dtoStores.add(mapper.map(eStore, XStoreDto.class)));
        return dtoStores;
    }

    @Transactional
    @Override
    public void addStore(XStoreDto storeDto) {
        storeDAO.getCrudHelper().insert(mapper.map(storeDto, EStore.class));
    }

    @Transactional
    @Override
    public void editStore(XStoreDto storeDto) {
        storeDAO.getCrudHelper().update(mapper.map(storeDto, EStore.class));
    }

    @Transactional
    @Override
    public void removeStore(int n) {
        storeDAO.getCrudHelper().deleteById(n);
    }
}
