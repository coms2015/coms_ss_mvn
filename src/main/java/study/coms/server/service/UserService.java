package study.coms.server.service;

import study.coms.exchange.user.XUserDto;

public interface UserService {
    XUserDto tryToAuth(String login, String password);

    void logout();
}
