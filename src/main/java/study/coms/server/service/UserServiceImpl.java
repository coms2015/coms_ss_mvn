package study.coms.server.service;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import study.coms.exchange.user.XUserDto;
import study.coms.server.dao.UserDAO;
import study.coms.server.entity.EUser;
import study.coms.server.security.UserSession;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserSession session;

    @Autowired
    private Mapper mapper;

    @Transactional
    @Override
    public XUserDto tryToAuth(String login, String password) {
        EUser user = userDAO.getUserByLoginAndPassword(login, password);
        if (user != null) {
            session.init(user);
            return mapper.map(user, XUserDto.class);
        }
        return null;
    }

    @Transactional
    @Override
    public void logout() {
        session.reset();
    }
}
